/** Represents the iContactPoints proxy adapter using jquery */
class jqueryiContactPointsProxyAdapter {
  constructor() { }
  
  post(url, data, settings, callback) {
    if (!settings) {
      settings = { };
    }
    
    settings.type = 'POST';
    settings.url = url;
    settings.data = data;
    
    $.ajax(settings)
      .done(function (data) {
        if (callback) callback(null, data);
      })
      .fail(function (err) {
        if (callback) callback(err);
    });
  }
}