// 0. Core Classes
/** Represents the iContactPoints configuration providing an api endpoint */
class iContactPointsConfig {
  constructor() {
    let protocol = "http";
    let uri = "api.icontactpoints.com";
    let version = 1;
    
    this.apiEP = protocol + '://' + uri + '/v' + version;
  }
}

/** Represents the iContactPoints client settings */
class iContactPointsClientSettings {
  constructor(apiEP, appAuthID, proxy) {
    this.apiEP = apiEP;
    this.appAuthID = appAuthID;
    this.proxy = proxy;
  }
}

/** Represents the iContactPoints client */
class iContactPointsClient {
  constructor(appAuthID, proxy, config) {
    if (!appAuthID) throw new Error('No appAuthID provided. The iContactPointsClient needs the appAuthID.');
    
    if (!proxy) {
      if (typeof jqueryiContactPointsProxyAdapter === 'function') {
        proxy = new jqueryiContactPointsProxyAdapter();
      } else {
        throw new Error('No proxy adapter provided. The iContactPointsClient needs the proxy adapter.');
      }
    }
    
    if (!config) {
      config = new iContactPointsConfig();
    }
    
    this.settings = new iContactPointsClientSettings(config.apiEP, appAuthID, proxy);
    this.authClient = new AuthClient(this.settings);
  }

  /** Gets the AuthClient instance. */
  getAuthClient() {
    return this.authClient;
  }
  
  /** Resolves an instance of iContactPoints configuartion. */
  static resolveConfig() {
    return new iContactPointsConfig(); 
  }
}

// 1. Authentication and Authorization
/** Represents the authentication and authorization token */
class AuthToken {
  /** Initializes the AuthToken class */
  constructor() {
    this.cref = '';
    this.accessToken = '';
  }
}

/** Represents the authentication and authorization client proxy */
class AuthClient {
  constructor(settings) {
    this.token = new AuthToken();
    this.settings = settings;
  }
  
  /** Sign in with email address */
  signInByEmail(email, password, callback) {
    let url = this.settings.apiEP + '/authen/email';
    let data = { appAuthID: this.settings.appAuthID, email: email, password: password };
    
    this.doSignIn(url, data, callback, this.settings.proxy);
  }
  
  /** Sign in with mobile number */
  signInByMobile(mobile, password, callback) {
    let url = this.settings.apiEP + '/authen/mobile';
    let data = { appAuthID: this.settings.appAuthID, mobile: mobile, password: password };
    
    this.doSignIn(url, data, callback, this.settings.proxy);
  }
  
  /** Internal use for signing in */
  doSignIn(url, data, callback, proxy) {
    let authToken = this.token;
    
    // proxy.post(url:string, data:object, settings:object, callback:Function);
    proxy.post(url, data, null, function (err, result) {
      if (err) {
        authToken.cref = null;
        authToken.accessToken = null;
        
        if (callback) callback(err);
        
      } else if (result) {
        
        if (result.success) {
          authToken.cref = result.cref;
          authToken.accessToken = result.accessToken;
        } else {
          authToken.cref = null;
          authToken.accessToken = null;
        }
        
        if (callback) callback(null, result);
      }
    });
  }
  
  /** Sign up contact and access */
  signUp(mobile, email, firstname, lastname, gender, countrycode, password, callback) {
    let contactURL = this.settings.apiEP + '/contacts';
    let accessURL = this.settings.apiEP + '/contactAccesses';
    
    let data = { mobile: mobile, email: email, firstname: firstname, lastname: lastname, gender: gender, countrycode: countrycode };
    let proxy = this.settings.proxy;
    let signUpContact = new Promise(function(resolve, reject) {
      proxy.post(contactURL, data, null, function (err, result) {
        if (err) {
          reject(err)
        }
        else if (result) {
          
          if (result.success) {
            resolve(result);
          } else {
            reject(result);
          }
        }
        else {
          reject(new Error("Unknown error was thrown when creating a new contact for signup."));
        }
      });
    });
    
    signUpContact.then(function (result) {
      let contact = result;
      let data = { contact_refex: result.refex, password: password };
      
      proxy.post(accessURL, data, null, function(err, result) {
        if (err) {
          if (callback) callback(err);
        } else if (result) {
          
          if (result.success) {
            if (callback) callback(null, contact);  // Returns the result of Contact, instead of ContactAccess
          } else {
            if (callback) callback(result);
          } 
        } else {
          if (callback) callback(new Error("Unknown error was thrown when creating a new contact access for signup."));
        }
      });
    })
    .catch (function (reject) {
      if (callback) callback(reject);
    });
  }
  
  /** Verify the current access token */
  verifyToken(callback) {
    if (!this.token.cref || !this.token.accessToken) {
      callback(null, { Success:false, Status:'Unauthenticated' })
    } else {
      let url = this.settings.apiEP + '/authen/verify'
      let data = { appAuthID: this.settings.appAuthID, contact_refex: this.token.cref, accessToken: this.token.accessToken };
      let proxy = this.settings.proxy;
      
      proxy.post(url, data, null, function(err, result) {
        if (err) {
          if (callback) callback(err);
        } else {         
          if (callback) callback(null, result);
        }
      });
    }
  }
}